﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleHpPack : CollectibleItem
{

    public override void CollectItem()
    {

        if (OnCollectSound)
        {
            Source.PlayOneShot(OnCollectSound, Scene.EffectsVolume);
        }

        PlayerController.main.TakeDamage(-ScoreValue);
        gameObject.SetActive(false);
        if (HasDisappearEffect)
        {
            GameObject g = Scene.CollectEffectsContainer.GetObject();
            g.transform.position = transform.position;
            ParticleSystem.MainModule mm = g.GetComponent<ParticleSystem>().main;
            mm.startColor = DisappearEffectColor;
        }
    }

}

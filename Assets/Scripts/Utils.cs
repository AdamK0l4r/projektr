﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Utils
{

    [Serializable]
    public struct HitColliderData
    {

        [SerializeField]
        float radius;

        [Min(1)]
        [SerializeField]
        int detail;

        [SerializeField]
        float height;

        public float Radius { get { return radius; } set { radius = value; } }

        public int Detail { get { return detail; } set { detail = value; if (detail < 1) detail = 1; } }

        public float Height { get { return height; } set { height = value; } }

        public HitColliderData(float radius, int detail, float height)
        {
            this.radius = radius;
            this.detail = detail;
            this.height = height;

            if (this.detail < 1)
                this.detail = 1;
        }

    }

    public static Mesh GenerateHitMesh(HitColliderData data)
    {
        Mesh mesh = new Mesh();

        List<Vector3> verts = new List<Vector3>((data.Detail + 2) * 2);
        List<int> tris = new List<int>();

        float step = 1f / (data.Detail + 1) * data.Radius * 2f;
        float yPos = data.Height / 2f;

        //top base verts and tris
        for (int i = 0; i < data.Detail + 2; i++)
        {
            float x = i * step;
            verts.Add(new Vector3(x - data.Radius, yPos, halfCircle(x)));
        }

        int end = data.Detail + 1;
        //top base tris
        for (int i = 0; i < data.Detail; i++)
        {
            AddTriangle(i + 1, end, i);
        }

        int cc = verts.Count;
        //down verts
        for (int i = 0; i < cc; i++)
        {
            verts.Add(new Vector3(verts[i].x, -yPos, verts[i].z));
        }

        //down tris
        end = verts.Count - 1;
        //top base tris
        for (int i = data.Detail + 2; i < (data.Detail + 2) * 2 - 2; i++)
        {
            AddTriangle(i, end, i + 1);
        }

        //backTris
        int down = 0, upper = data.Detail + 2;
        int downEnd = data.Detail + 1, upperEnd = (data.Detail + 2) * 2 - 1;

        AddTriangle(downEnd, upper, down);
        AddTriangle(upper, downEnd, upperEnd);

        //side tris

        for (down = 0; down < data.Detail + 1; down++, upper++)
        {
            AddTriangle(down, upper, down + 1);
            AddTriangle(down + 1, upper, upper + 1);
        }

        mesh.SetVertices(verts);
        mesh.SetTriangles(tris, 0);
        mesh.RecalculateNormals();

        return mesh;

        float halfCircle(float x)
        {
            //upper half circle with x offset of 1
            return Mathf.Sqrt(data.Radius * data.Radius - (x - data.Radius) * (x - data.Radius));
        }

        void AddTriangle(int v1, int v2, int v3)
        {
            tris.Add(v1);
            tris.Add(v2);
            tris.Add(v3);
        }

    }


}

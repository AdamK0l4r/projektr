﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ScoreShow : MonoBehaviour
{

    public Text outputText;

    public string scoreFile = "Scores.bin";

    void LoadAndShowData()
    {
        //print(Application.persistentDataPath);
        ScoreData data = ScoreData.Load(Path.Combine(Application.persistentDataPath, scoreFile));
        if(data != null)
        {
            StringBuilder sb = new StringBuilder();
            if(data.LevelDatas.Length < 1)
            {
                sb.Append("No score for any of levels");
            } else
            {
                for(int i = 0; i < data.LevelDatas.Length; i++)
                {
                    sb.Append(data.LevelDatas[i].Name).Append("\t\t\t\t").Append(data.LevelDatas[i].Score).Append('\n');
                }
            }
            outputText.text = sb.ToString();
        } else
        {
            outputText.text = "No score for any of levels";
        }
    }

    private void OnEnable()
    {
        LoadAndShowData();
    }

    private void OnDisable()
    {
        outputText.text = "";
    }

}

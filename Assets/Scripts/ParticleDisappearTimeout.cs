﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDisappearTimeout : MonoBehaviour
{

    public float Timeout = 1f;

    ParticleSystem ps;

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
    }


    private void OnEnable()
    {
        ps.Play();
        StartCoroutine(TimeoutTimer());
    }

    IEnumerator TimeoutTimer()
    {
        yield return new WaitForSeconds(Timeout);
        ps.Stop();
        Scene.CollectEffectsContainer.ReturnObject(gameObject);
        gameObject.SetActive(false);
        yield break;
    }

}

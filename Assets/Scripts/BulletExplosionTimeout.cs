﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplosionTimeout : MonoBehaviour
{

    public float Timeout = 3f;

    ParticleSystem ps;

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
    }


    private void OnEnable()
    {
        ps.Play();
        StartCoroutine(TimeoutTimer());
    }

    IEnumerator TimeoutTimer()
    {
        yield return new WaitForSeconds(Timeout);
        ps.Stop();
        Scene.ExplosionsContainer.ReturnObject(gameObject);
        gameObject.SetActive(false);
        yield break;
    }

}

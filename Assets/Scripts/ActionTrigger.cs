﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionTrigger : MonoBehaviour
{

    public UnityEvent onPlayerEnter;
    public UnityEvent onPlayerExit;

    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (onPlayerEnter != null && player)
        {
          //  print("Player Enter");
            onPlayerEnter.Invoke();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (onPlayerExit != null && player)
        {
          //  print("Player Exit");
            onPlayerExit.Invoke();
        }
    }
}

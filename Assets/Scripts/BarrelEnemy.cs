﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelEnemy : EnemyController
{
    public float dissapearTime = 3f;
    public float explosionSpeed = 5f;
    public float explosionSpeedTime = 5f;
    public AnimationCurve explosionVelocityCurve;
    public AudioClip ExplosionSound;
    public GameObject HitCollider;
    public GameObject ExplosionCollider;
    public ParticleSystem Explosion;
    public AudioSource AudioSource;

    public Animator anim;

    public void OnTriggerEnter(Collider other)
    {
        if (EnemyState.State != EnemyStateEnum.Die)
        {
            //print(other.name);
            PlayerController p = other.GetComponent<PlayerController>();
            if (p != null)
            {
                //p.AddOuterVelocity((p.transform.position - transform.position), explosionSpeed, explosionSpeedTime, explosionVelocityCurve);
                p.Jump(true);
                BarrelDie();
            }
        }
    }

    public void OnHitDamageColliderHit(Collider other)
    {
        //print(other.name);
        PlayerController p = other.GetComponent<PlayerController>();
        if (p != null)
        {
            //p.AddOuterVelocity((p.transform.position - transform.position), explosionSpeed, explosionSpeedTime, explosionVelocityCurve);
            BarrelExplode();
        }
    }

    public void OnExplosionColliderEnter(Collider other)
    {
        ExplosionCollider.SetActive(false);
        PlayerController p = other.GetComponent<PlayerController>();
        if (p != null)
        {
            p.AddOuterVelocity((p.transform.position - transform.position), explosionSpeed, explosionSpeedTime, explosionVelocityCurve);
        }
    }

    public override void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
            BarrelExplode();
    }

    public void BarrelExplode()
    {
        //print("Barrel Explodes");
        AudioSource.PlayOneShot(ExplosionSound, Scene.EffectsVolume);
        ExplosionCollider.SetActive(true);
        Explosion.gameObject.SetActive(true);
        Explosion.Play();
        BarrelDie();
    }
    IEnumerator dissapear()
    {
        yield return new WaitForSeconds(dissapearTime);
        gameObject.SetActive(false);
    }
    public void BarrelDie()
    {
        anim.SetBool("Dead", true);
        health = 0;
        EnemyState.State = EnemyStateEnum.Die;
        HitCollider.SetActive(false);
        agent.velocity = Vector3.zero;
        agent.ResetPath();
        StartCoroutine(dissapear());

    }
    protected override void Die()
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitToKillBox : MonoBehaviour, IHittable
{

    public int hp = 2;

    private int startHp;
    private MeshRenderer mr;

    // Start is called before the first frame update
    void Start()
    {
        startHp = hp;
        mr = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int loss)
    {
        hp -= loss;
        ColorAlphaMaterial();
        if(hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void ColorAlphaMaterial()
    {
        if(startHp != 0)
        {
            mr.material.color = new Color(mr.material.color.r, mr.material.color.g, mr.material.color.b, Mathf.Max(0, hp) / (float)startHp);
        }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[RequireComponent(typeof(Rigidbody))]
public class MovingPlatform : MonoBehaviour
{

    public enum PlatformType
    {
        Loop,
        PingPong
    }

    [Serializable]
    public struct PlatformPoint
    {
        public float TransitionTime;
        public float WaitTime;
        public Vector3 Position;
    }

    public PlatformType Type;

    public PlatformPoint[] Points;

    public int currentPoint = 0;

    /// <summary>
    /// User for PingPongMode
    /// </summary>
    bool up = true;

    Vector3 startPos = Vector3.negativeInfinity;

    new Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.interpolation = RigidbodyInterpolation.Extrapolate;
        rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        rigidbody.freezeRotation = true;
        rigidbody.useGravity = false;
        rigidbody.mass = float.MaxValue;

        startPos = rigidbody.position;
        StartCoroutine(MoveRoutine());
    }

    public static readonly float kEpsilon = 0.00001f;

    IEnumerator MoveRoutine()
    {
        while (true)
        {
            //moving
            Vector3 targetPosition = Points[currentPoint].Position + startPos;
            float distance = Vector3.Distance(targetPosition, rigidbody.position);
            while (Vector3.Distance(rigidbody.position, targetPosition) > kEpsilon)
            {
                Vector3 movement = Vector3.MoveTowards(rigidbody.position, targetPosition, distance / Points[currentPoint].TransitionTime * Time.fixedDeltaTime);
                rigidbody.velocity = (movement - rigidbody.position) / Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            //waiting
            rigidbody.velocity = Vector3.zero;
            yield return new WaitForSeconds(Points[currentPoint].WaitTime);
            
            //transition
            switch (Type)
            {
                case PlatformType.Loop:
                    currentPoint = (currentPoint + 1) % Points.Length;
                    break;
                case PlatformType.PingPong:
                    if (up)
                    {
                        if (currentPoint + 1 < Points.Length)
                        {
                            currentPoint++;
                        }
                        else
                        {
                            up = false;
                            currentPoint--;
                        }
                    }
                    else
                    {
                        if (currentPoint - 1 >= 0)
                        {
                            currentPoint--;
                        }
                        else
                        {
                            up = true;
                            currentPoint++;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

#if UNITY_EDITOR

    public bool drawPoints = true;

    private void OnDrawGizmos()
    {

        if (drawPoints)
        {

            if (!EditorApplication.isPlaying)
            {
                startPos = transform.position;
            }

            if (Points != null)
            {
                Gizmos.color = Color.blue;
                for (int i = 0; i < Points.Length; i++)
                {
                    if (currentPoint != i)
                    {
                        Gizmos.DrawSphere(startPos + Points[i].Position, 0.25f);
                    }
                    else
                    {
                        Gizmos.color = Color.green;
                        Gizmos.DrawSphere(startPos + Points[i].Position, 0.25f);
                        Gizmos.color = Color.blue;
                    }
                }

                switch (Type)
                {
                    case PlatformType.Loop:
                        for (int i = 0; i < Points.Length; i++)
                        {
                            if((i + 1) % Points.Length == currentPoint)
                            {
                                Gizmos.color = Color.green;
                            } else
                            {
                                Gizmos.color = Color.blue;
                            }
                            Gizmos.DrawLine(startPos + Points[i].Position, startPos + Points[(i + 1) % Points.Length].Position);
                        }
                        break;
                    case PlatformType.PingPong:
                        for (int i = 0; i < Points.Length - 1; i++)
                        {
                            if (up && (i + 1) == currentPoint || !up && i == currentPoint)
                            {
                                Gizmos.color = Color.green;
                            }
                            else
                            {
                                Gizmos.color = Color.blue;
                            }
                            Gizmos.DrawLine(startPos + Points[i].Position, startPos + Points[i + 1].Position);
                        }
                        break;
                    default:
                        break;
                }

            }

        }
    }

#endif

}

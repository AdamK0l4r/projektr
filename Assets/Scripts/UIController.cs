﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public Text ScoreText;

    public GameObject WinUI;
    public GameObject LoseUI;

    public GameObject pausedUI;

    public static UIController main;


    private void Awake()
    {
        main = this;
    }

    private void Update()
    {
        
    }

    public void RedrawScore()
    {
        ScoreText.text = "Score: " + Scene.main.Score;
    }

    public void ShowWinUI()
    {
        WinUI.SetActive(true);
    }

    public void ShowLoseUI()
    {
        LoseUI.SetActive(true);
    }

    public void ShowPausedUI()
    {
        pausedUI.SetActive(true);
    }

    public void HidePausedUI()
    {
        pausedUI.SetActive(false);
    }

    public void Continue()
    {
        Scene.main.Pause();
    }

    public void Menu()
    {
        Scene.main.LoadMenu();
    }

    public void NextLevel()
    {
        Scene.main.LoadNextLevel();
    }

    public void Restart()
    {
        Scene.main.ReloadLevel();
    }

}

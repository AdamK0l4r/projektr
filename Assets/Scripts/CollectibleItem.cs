﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleItem : MonoBehaviour
{
    // Start is called before the first frame update
    public int ScoreValue = 1;
    public Vector3 AngularVelocity = Vector3.one;

    public bool HasDisappearEffect;
    public Color DisappearEffectColor;

    public AudioSource Source;

    public AudioClip OnCollectSound;

    void Start()
    {
        if (!Source)
            Source = Scene.DefaultSoruce;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(AngularVelocity * Time.deltaTime);
    }
    public virtual void CollectItem()
    {
        Scene.main.Score += ScoreValue;

        if(OnCollectSound)
        {
            Source.PlayOneShot(OnCollectSound, Scene.EffectsVolume);
        }

        gameObject.SetActive(false);
        if(HasDisappearEffect)
        {
            GameObject g = Scene.CollectEffectsContainer.GetObject();
            g.transform.position = transform.position;
            ParticleSystem.MainModule mm = g.GetComponent<ParticleSystem>().main;
            mm.startColor = DisappearEffectColor;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Svi objekti koji se mogu "udariti" moraju implementirati ovaj interface
/// </summary>
public interface IHittable 
{

    /// <summary>
    /// Poziva se kada je objekt "udaren"
    /// </summary>
    /// <param name="damage">Šteta koju objekt dobiva</param>
    void TakeDamage(int damage);

}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Scene : MonoBehaviour
{
    
    public static Scene main;

    public ObjectHolder explosionsContianer;

    public ObjectHolder collectEffectsContainer;

    public string currentLevelName = "Level";

    public string menuSceneName = "Menu";

    public string nextLevelName;

    public string scoreFile = "Scores.bin";

    public static float EffectsVolume = 0.7f;
    public static float MusicVolume = 0.5f;

    public AudioSource defaultSrouce;

    public AudioSource musicSource;

    public bool Final = false;

    public Button NextLevelButton;
    public GameObject GameFinishedObject;

    public static AudioSource DefaultSoruce { get => main.defaultSrouce; }

    public static ObjectHolder ExplosionsContainer { get { return main.explosionsContianer; } }

    public static ObjectHolder CollectEffectsContainer { get { return main.collectEffectsContainer; } }

    public string musicKey = "Music";
    public string effectsKey = "Effects";

    private void Awake()
    {
        main = this;
        Time.timeScale = 1f;
        if (currentLevelName != "Menu")
            Cursor.visible = false;
    }

    private void Start()
    {

        if (PlayerPrefs.HasKey(musicKey))
        {
            MusicVolume = PlayerPrefs.GetFloat(musicKey);
        }

        if (PlayerPrefs.HasKey(effectsKey))
        {
            EffectsVolume = PlayerPrefs.GetFloat(effectsKey);
        }

        musicSource.volume = MusicVolume;
        musicSource.Play();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    [SerializeField]
    int score;
    public int Score { get { return score; } set { if (value != score) { score = value; UIController.main.RedrawScore(); } } }

    bool paused = false;

    public bool Paused { get => paused; }

    public void Pause()
    {
        if (PlayerController.main.PlayerState.State == PlayerController.PlayerStateEnum.Dead)
            return;
        paused = !paused;
        if(paused)
        {
            Cursor.visible = true;
            UIController.main.ShowPausedUI();
            Time.timeScale = 0f;
        } else
        {
            Cursor.visible = false;
            UIController.main.HidePausedUI();
            Time.timeScale = 1f;
        }
    }

    public void PlayerWin()
    {
        Cursor.visible = true;
        //print("Player won");
        Time.timeScale = 0f;
        UIController.main.ShowWinUI();
        if(Final)
        {
            GameFinishedObject.SetActive(true);
            NextLevelButton.interactable = false;
        }
        ScoreData data = ScoreData.Load(Path.Combine(Application.persistentDataPath, scoreFile));
        if(data != null && data.LevelDatas.Length > 0)
        {
            ScoreData.LevelData d = null;
            for(int i = 0; i < data.LevelDatas.Length; i++)
            {
                if(data.LevelDatas[i].Name == currentLevelName)
                {
                    d = data.LevelDatas[i];
                    break;
                }
            }
            if(d == null)
            {
                List<ScoreData.LevelData> datas = new List<ScoreData.LevelData>(data.LevelDatas);
                d = new ScoreData.LevelData
                {
                    Name = currentLevelName,
                    Score = score
                };
                datas.Add(d);
                data.LevelDatas = datas.ToArray();
                data.Save(Path.Combine(Application.persistentDataPath, scoreFile));
            } else
            {
                if(d.Score < score)
                {
                    d.Score = score;
                    data.Save(Path.Combine(Application.persistentDataPath, scoreFile));
                }
            }
        } else
        {
            data = new ScoreData();
            data.LevelDatas = new ScoreData.LevelData[1];
            data.LevelDatas[0] = new ScoreData.LevelData();
            data.LevelDatas[0].Name = currentLevelName;
            data.LevelDatas[0].Score = score;
            data.Save(Path.Combine(Application.persistentDataPath, scoreFile));
        }
    }

    public void PlayerLose()
    {
        Cursor.visible = true;
        //print("Player lost");
        UIController.main.ShowLoseUI();
    }

    public void LoadNextLevel()
    {
        if(!string.IsNullOrEmpty(nextLevelName))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(nextLevelName);
        }
    }

    public void LoadMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(menuSceneName);
    }

    public void ReloadLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(currentLevelName);
    }

    public void SetMusicVolume(float v)
    {
        MusicVolume = v;
        if (v == 0f && main != null && main.musicSource != null)
            main.musicSource.Stop();
        PlayerPrefs.SetFloat(main.musicKey, MusicVolume);
        //print("Set music: " + v);
    }

    public void SetEffectsVolume(float v)
    {
        EffectsVolume = v;
        if(main != null)
        {
            PlayerPrefs.SetFloat(main.effectsKey, EffectsVolume);
        }
    }

}

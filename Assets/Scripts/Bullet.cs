﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : HitCollider
{

    public float lifeTime = 4f;

    ParticleSystem ps;

    Rigidbody rig;

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
        rig = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    public void Fire(Vector3 from, Vector3 to, float shootHeight)
    {


        float XZDistance = Vector3.Distance(new Vector3(from.x, 0f, from.z), new Vector3(to.x, 0f, to.z));
        float YDistance = Mathf.Abs(from.y - to.y);

        if(YDistance - shootHeight > 0f)
        {
            shootHeight = YDistance; //??
            //return;
        }

        gameObject.SetActive(true);
        transform.position = from;
        StartCoroutine(TimedDisable());
        ps.Play();

        

        Vector3 direction = (to - from).normalized;

        Vector3 xzDir = direction * (XZDistance / Mathf.Sqrt(-2 * shootHeight / Physics.gravity.y) + Mathf.Sqrt(2 * (YDistance - shootHeight) / Physics.gravity.y));
        Vector3 yDir = Vector3.up * Mathf.Sqrt(-2 * Physics.gravity.y * shootHeight);

        rig.velocity = xzDir + yDir;

    }

    IEnumerator TimedDisable()
    {
        yield return new WaitForSeconds(lifeTime);
        if (ps)
            ps.Stop();
        gameObject.SetActive(false);
        yield break;
    }

    private void OnTriggerEnter(Collider other)
    {
        //print(gameObject.name + " hit: " + other.name);
        IHittable obj = other.GetComponent<IHittable>();
        if (obj != null)
        {
            obj.TakeDamage(damage);
            if (onCollide != null)
                onCollide.Invoke(other);
        }

        PlayerController p = other.GetComponent<PlayerController>();
        if(p != null)
        {
            GameObject expl = Scene.ExplosionsContainer.GetObject();
            expl.transform.position = transform.position;
            gameObject.SetActive(false);
#if UNITY_EDITOR
            //EditorApplication.isPaused = true;
#endif
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{

    public float targetDistance = 5f;
    public float minDistance = 1f;
    public Vector3 offsetFromPlayer = new Vector3(0f, 1f, -5f);

    public float followSmoothTime = 0.2f;

    public Transform target;

    public Vector3 targetLookAtOffset;
    public float lookAtSmoothTime = 0.2f;

    public float yMin = -10f, yMax = 80f;

    public float mx, my;

    public float sphereCastRadius = 0.3f;
    public LayerMask sphereCastLayers;

    public float distanceMoveTime = 0.5f;
    public float distanceMoveTimeDiv = 10f;

    public float actualDistance;

    void Start()
    {
        actualDistance = targetDistance;
        if(!target)
        {
            target = FindObjectOfType<PlayerController>().transform;
        }
    }

    private void Update()
    {
        if (!Scene.main.Paused)
        {
            mx += Input.GetAxis("Mouse X");
            my = Mathf.Clamp(my - Input.GetAxis("Mouse Y"), yMin, yMax);
        }
    }

    Vector3 sdVel;
    Vector3 sdaVel;

    Vector3 smoothDampAngles(Vector3 current, Vector3 target, ref Vector3 vel, float smoothTime)
    {
        float x = Mathf.SmoothDampAngle(current.x, target.x, ref vel.x, smoothTime),
              y = Mathf.SmoothDampAngle(current.y, target.y, ref vel.y, smoothTime),
              z = Mathf.SmoothDampAngle(current.z, target.z, ref vel.z, smoothTime);

        if (x == target.x)
            vel.x = 0;
        if (float.IsNaN(x))
            x = current.x;

        if (y == target.y)
            vel.y = 0;
        if (float.IsNaN(y))
            y = current.y;

        if (z == target.z)
            vel.z = 0;
        if (float.IsNaN(z))
            z = current.z;
        
        return new Vector3(x, y, z);

    }

    float distVel;
    // Update is called once per frame
    void LateUpdate()
    {

        if (!Scene.main.Paused)
        {

            RaycastHit hit = new RaycastHit();
            bool h = Physics.SphereCast(target.position, sphereCastRadius, transform.position - target.position,
                                        out hit, actualDistance, sphereCastLayers, QueryTriggerInteraction.Ignore);

            if (!h)
            {

                actualDistance = Mathf.SmoothDamp(actualDistance, targetDistance, ref distVel, distanceMoveTime);

            }
            else
            {

                //actualDistance = Mathf.SmoothDamp(actualDistance, Vector3.Distance(target.position, hit.point) - sphereCastRadius, ref distVel, distanceMoveTime / distanceMoveTimeDiv);
                actualDistance = Vector3.Distance(target.position, hit.point) - sphereCastRadius;

            }

            actualDistance = Mathf.Max(minDistance, actualDistance);


            Vector3 targetLookAtAngles = Quaternion.LookRotation(target.position + targetLookAtOffset - transform.position).eulerAngles;
            Vector3 finRot = smoothDampAngles(transform.eulerAngles, targetLookAtAngles, ref sdaVel, lookAtSmoothTime);
            //print("target: " + targetLookAtAngles + " smh: " + finRot);
            transform.eulerAngles = finRot;

            Quaternion lookRotation = Quaternion.Euler(my, mx, 0f);

            Vector3 targetPosition = lookRotation * offsetFromPlayer.normalized * actualDistance + target.position;

            Vector3 movement = Vector3.SmoothDamp(transform.position, targetPosition, ref sdVel, followSmoothTime);

            transform.position = movement;

        }

    }

    private void OnDrawGizmos()
    {

        Gizmos.color = new Color(1, 0, 0, 0.3f);

        Gizmos.DrawSphere(transform.position, sphereCastRadius);

    }

}

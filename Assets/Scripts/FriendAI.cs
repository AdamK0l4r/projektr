﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendAI : MonoBehaviour
{
    //TODO

    public float canShowMessageRange = 6f;
    public float canShowAnyMessageRange = 10f;

    public KeyCode showMessageKey = KeyCode.E;

    public GameObject messageObject;
    public GameObject nonShownObject;

    public bool talkOnce = true;

    bool shown = false;
    bool talked;

    private void Start()
    {
        messageObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(transform.position, PlayerController.main.transform.position);
        if(dist <= canShowMessageRange)
        {
            if(Input.GetKeyDown(showMessageKey))
            {
                Show();
            }
        } else
        {
            Hide();
        }

        if(shown)
        {
            messageObject.transform.LookAt(Camera.main.transform);
        } else if(nonShownObject && (talkOnce & !talked || !talkOnce))
        {
            nonShownObject.transform.LookAt(Camera.main.transform);
        }

        if(dist > canShowAnyMessageRange)
        {
            messageObject.SetActive(false);
            nonShownObject.SetActive(false);
        }

    }

    void Show()
    {
        if (!shown)
        {
            if (nonShownObject)
                nonShownObject.SetActive(false);
            messageObject.SetActive(true);
            shown = true;
            talked = true;
        } else
        {
            Hide();
        }
    }

    void Hide()
    {
        if (nonShownObject && (talkOnce & !talked || !talkOnce))
            nonShownObject.SetActive(true);
        messageObject.SetActive(false);
        shown = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{

    public GameObject IntroObjects;
    public GameObject LevelsObjects;
    public GameObject ScoresObjects;
    public GameObject OptionsObjects;

    public void DisableAllUI()
    {
        IntroObjects.SetActive(false);
        LevelsObjects.SetActive(false);
        ScoresObjects.SetActive(false);
        OptionsObjects.SetActive(false);
    }

    private void Start()
    {
        LoadMenuUI(IntroObjects);
    }

    public void LoadMenuUI(GameObject part)
    {
        DisableAllUI();
        part.SetActive(true);
    }

    public void LoadLevel(string levelName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
    }

    public void Exit()
    {
        Application.Quit(0);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

[System.Serializable]
public class ScoreData
{
    
    public class LevelData
    {
        public string Name;
        public int Score;
    }

    public LevelData[] LevelDatas;

    public bool Save(string path)
    {

        if (LevelDatas == null || path == null)
            return false;

        try
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
            {
                writer.Write("ROBY");
                writer.Write(LevelDatas.Length);
                for(int i = 0; i < LevelDatas.Length; i++)
                {
                    writer.Write(LevelDatas[i].Name);
                    writer.Write(LevelDatas[i].Score);
                }
            }
        } catch (Exception)
        {
            return false;
        }

        return true;

    }

    public static ScoreData Load(string path)
    {

        if (path == null)
            return null;

        ScoreData data = new ScoreData();

        try
        {
            using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                string s = reader.ReadString();
                if (s != "ROBY")
                    return null;
                
                int count = reader.ReadInt32();
                if (count < 0)
                    return null;

                data.LevelDatas = new LevelData[count];
                
                for(int i = 0; i < count; i++)
                {
                    data.LevelDatas[i] = new LevelData();
                    data.LevelDatas[i].Name = reader.ReadString();
                    data.LevelDatas[i].Score = reader.ReadInt32();
                }

            }
        } catch (Exception)
        {
            return null;
        }

        return data;

    }

}

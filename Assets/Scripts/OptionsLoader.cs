﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsLoader : MonoBehaviour
{

    public string musicKey = "Music";
    public string effectsKey = "Effects";

    public Slider musicSlider;
    public Slider effectsSlider;

    private void OnEnable()
    {
        if(PlayerPrefs.HasKey(musicKey))
        {
            musicSlider.value = PlayerPrefs.GetFloat(musicKey);
        } else
        {
            musicSlider.value = Scene.MusicVolume;
        }

        if (PlayerPrefs.HasKey(effectsKey))
        {
            effectsSlider.value = PlayerPrefs.GetFloat(effectsKey);
        } else
        {
            effectsSlider.value = Scene.EffectsVolume;
        }
    }

}

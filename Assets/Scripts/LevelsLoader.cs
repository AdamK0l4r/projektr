﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class LevelsLoader : MonoBehaviour
{

    [System.Serializable]
    public struct LevelData
    {
        public string PreviousLevel;
        public string LevelName;
    }

    public LevelData[] Levels;

    public Button[] LevelButtons;

    public string scoreFile = "Scores.bin";
    
    public void OnEnable()
    {
        ScoreData data = ScoreData.Load(Path.Combine(Application.persistentDataPath, scoreFile));
        
        for(int i = 0; i < Levels.Length; i++)
        {
            if (string.IsNullOrEmpty(Levels[i].PreviousLevel))
                continue;
            LevelButtons[i].interactable = DataHasLevel(Levels[i].PreviousLevel);
        }

        bool DataHasLevel(string LevelName)
        {
            if (data == null || data.LevelDatas == null)
                return false;
            for(int i = 0; i < data.LevelDatas.Length; i++)
            {
                if (data.LevelDatas[i].Name == LevelName)
                    return true;
            }
            return false;
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Gate : MonoBehaviour
{
    public Vector3 OpenOffset;
    private Vector3 StartPosition;
    public float OpenTime = 1f;
    public bool open = false;
    new Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.mass = float.MaxValue;
        rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        StartPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    bool moving = false;
    IEnumerator Move() {
        if (!moving)
        {

            //print("Moving");
            moving = true;
            Vector3 TargetPos;
            if (!open)
            {
                TargetPos = StartPosition;

            }
            else
            {
                TargetPos = StartPosition + OpenOffset;
            }
            float distance = Vector3.Distance(rigidbody.position, TargetPos);
            while (Vector3.Distance(rigidbody.position, TargetPos) > 0.001f)
            {
               // print("Move");
                Vector3 movement = Vector3.MoveTowards(rigidbody.position, TargetPos, (distance / OpenTime) * Time.fixedDeltaTime);
                rigidbody.velocity = (movement - rigidbody.position) / Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
            rigidbody.velocity = Vector3.zero;
            moving = false;
        }
    //    print("Moving Done");
        yield break;

    }

    public void OpenGate()
    {
        if (!open && !moving)
        {
            open = true;
            StartCoroutine(Move());


        }
    }
    public void CloseGate()
    {
        if (open && !moving)
        {
            open = false;
            StartCoroutine(Move());
        }
    }
#if UNITY_EDITOR
    public bool DrawDebug = true;
    public void OnDrawGizmos()
    {
        if (DrawDebug)
        {
            if (!EditorApplication.isPlaying)
            {
                StartPosition = transform.position;
            }
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(StartPosition+OpenOffset,0.1f);
        }
    }
#endif
}

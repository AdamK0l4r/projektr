﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHpUI : MonoBehaviour
{

    /// <summary>
    /// Koliko hp-a jedan hpTile može maksimalno prikazati 
    /// </summary>
    [Min(1)]
    public int maxHpPerTile = 2;

    /// <summary>
    /// Sličice za stanje 
    /// </summary>
    public Sprite[] hpTileImages;

    /// <summary>
    /// Objekt sa komponentom slike koji se instancira kao hpTile
    /// </summary>
    public Image hpTile;

    /// <summary>
    /// Lista svih hp tile-ova
    /// </summary>
    List<Image> tiles;

    /// <summary>
    /// Trenutno stanje hp-a
    /// </summary>
    [SerializeField]
    int hp = 0;

    /// <summary>
    /// Za dohvat i modificiranje hp-a
    /// pri modifikaciji uređuje i objekt
    /// </summary>
    public int Hp {  
        get { 
            return hp; 
        } 
        set {
            if (hp != value) { 
                hp = value; 
                DrawHp(); 
            } 
        } 
    }

    /// <summary>
    /// Za validaciju u editor-u
    /// </summary>
    public void OnValidate()
    {
        if (hpTileImages == null)
            hpTileImages = new Sprite[maxHpPerTile];
        if(hpTileImages.Length < maxHpPerTile + 1)
        {
            System.Array.Resize(ref hpTileImages, maxHpPerTile + 1);
        }
    }

    /// <summary>
    /// Postavlja hpTile.ove za prikaz hp-a
    /// </summary>
    void DrawHp()
    {

        int hp = this.hp;

        if(tiles == null)
        {
            tiles = GetComponentsInChildren<Image>().ToList();
        }

        int t = Mathf.Max(Mathf.CeilToInt(hp / (float)maxHpPerTile), tiles.Count);
        for(int i = 0; i < t; i++)
        {
            //stvori novi tile ako nema mjesta za popunit hp
            if(i >= tiles.Count)
            {
                Image img = Instantiate(hpTile);
                img.transform.SetParent(transform);
                tiles.Add(img);
            }

            Sprite targetImg = hpTileImages[Mathf.Clamp(hp, 0, maxHpPerTile)];
            if (tiles[i].sprite != targetImg)
            {
                tiles[i].sprite = targetImg;
            }

            hp = Mathf.Max(0, hp -= maxHpPerTile);
        }
    }

}

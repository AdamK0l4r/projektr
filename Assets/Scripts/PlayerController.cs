﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngineInternal;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour, IHittable
{
    public enum PlayerStateEnum { 
        Alive,
        Dead,
        Uncontrollable
    };

    public void TakeDamage(int damage)
    {
        if (PlayerState.State != PlayerStateEnum.Dead)
        {
            if (damage < 0)
                anim.SetBool("GotHit", true);
            health -= damage;
            //print("Player took damage");
            if (health <= 0)
            {
                PlayerState.State = PlayerStateEnum.Dead;
                //print("You have died!");
            }
        }
    }

    public StateMachine<PlayerStateEnum> PlayerState = new StateMachine<PlayerStateEnum>(PlayerStateEnum.Alive);
    new Rigidbody rigidbody;

    public int health = 6;

    public float speed = 4f;

    public float jumpVelocity = 5f;

    public float jumpTimerLimit = 0.2f;

    public KeyCode jumpKey = KeyCode.Space;

    public bool onGround = false;

    public float hitTimerLimit = 0.5f;

    public KeyCode hitKey = KeyCode.Mouse0;

    public Vector3 outerVelocity;

    public float sphereCastRadius = 0.4f;
    public float sphereCastDistance = 0.52f;
    public LayerMask groundLayers;

    public LayerMask platformsLayer;

    public HitCollider hitCollider = null;

    public static PlayerController main;

    public float uncontrollableGravityModifier = 15f;

    PlayerHpUI hpUI;

    public Utils.HitColliderData hitColliderData;

    public Animator anim;

    public PlayerSound playerSound;

    PlayerCamera playerCamera;
    public Vector3 otherSources;
    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        rigidbody.freezeRotation = true;
        rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        rigidbody.useGravity = false;
        
        playerCamera = FindObjectOfType<PlayerCamera>();

        if(hitCollider == null)
        {
            generateHitCollider();
        }

        hpUI = FindObjectOfType<PlayerHpUI>();
        PlayerState.Methods[PlayerStateEnum.Alive] = Alive;
        PlayerState.Methods[PlayerStateEnum.Dead] = Dead;
        PlayerState.Methods[PlayerStateEnum.Uncontrollable] = Uncontrollable;

        PlayerState.StateEnterMethods[PlayerStateEnum.Dead] = PlayerDie;

        playerSound = GetComponentInChildren<PlayerSound>();

    }

    float xInput, yInput;
    bool jumpInput;
    bool refreshInput = true;
    private void Update()
    {
        if (refreshInput)
        {
            xInput = Input.GetAxisRaw("Horizontal");
            yInput = Input.GetAxisRaw("Vertical");
            jumpInput = Input.GetKey(jumpKey);

            refreshInput = false;
        }
        if(hpUI)
        {
            hpUI.Hp = health;
        }
    }

    Vector3 inputVel;
    void Alive()
    {
        RaycastHit hit;
        onGround = Physics.SphereCast(transform.position, sphereCastRadius, Vector3.down, out hit, sphereCastDistance, groundLayers, QueryTriggerInteraction.Ignore);
        //  print("on ground: " + onGround);

        anim.SetBool("OnGround", onGround);
        anim.SetBool("GotHit", false);

        Debug.DrawLine(hit.point, hit.point + hit.normal, Color.cyan);

        //rigidbody.velocity += Physics.gravity * Time.fixedDeltaTime;

        Quaternion cameraOrientation = Quaternion.Euler(0f, playerCamera.transform.eulerAngles.y, 0f);

        Vector3 xzInput = new Vector3(xInput, 0, yInput).normalized;

        inputVel = cameraOrientation * xzInput * speed + Vector3.up * rigidbody.velocity.y;

        anim.SetBool("Running", xzInput != Vector3.zero & !didHit);

        if (xzInput != Vector3.zero && !didHit)
        {
            Quaternion orientation = Quaternion.LookRotation(cameraOrientation * xzInput);
            rigidbody.MoveRotation(orientation);
        }

        if (onGround)
        {

            processGround(hit);

            if (Input.GetKey(hitKey) && !didHit)
            {
                StartCoroutine(doHit());
            }

            if (!jumpTimerTiming)
                inputVel.y = 0f;

            if (jumpInput)
            {
                Jump();
                //print("jump");
            }

        }

        if (didHit)
            inputVel = Vector3.zero;

        outerVelocity = platformVelocity  + otherSources;

        platformVelocity = Vector3.zero;

        otherSources = Vector3.zero;

        rigidbody.velocity = inputVel + outerVelocity;

        rigidbody.velocity += Physics.gravity * Time.fixedDeltaTime;

        refreshInput = true;

#if UNITY_EDITOR

        if (drawPath)
        {
            posPast[posIndex] = transform.position;
            posIndex = (posIndex + 1) % posPast.Length;
        }

#endif

    }
    void Dead()
    {

        anim.SetBool("GotHit", false);
        if (!anim.GetBool("Dead"))
            anim.SetBool("Dead", true);

        RaycastHit hit;
        onGround = Physics.SphereCast(transform.position, sphereCastRadius, Vector3.down, out hit, sphereCastDistance, groundLayers, QueryTriggerInteraction.Ignore);
        if (!onGround)
            rigidbody.velocity += Physics.gravity * Time.deltaTime;
        else
            rigidbody.velocity = Vector3.zero;

    }

    void Uncontrollable()
    {
        anim.SetBool("GotHit", true);
        outerVelocity += otherSources;
        rigidbody.velocity = outerVelocity + Physics.gravity * uncontrollableGravityModifier * Time.fixedDeltaTime;
        outerVelocity = Vector3.zero;
        otherSources = Vector3.zero;
        //print("unc");
    }

    public void Jump(bool forceJump = false)
    {
        if (PlayerState.State == PlayerStateEnum.Alive)
        {
            if (forceJump)
            {
                if (currentJump != null)
                    StopCoroutine(currentJump);
                rigidbody.velocity = new Vector3(rigidbody.velocity.x, jumpVelocity, rigidbody.velocity.z);
                //inputVel.y = jumpVelocity;
                StartCoroutine(currentJump = jumpTimer());
                //print("force jumped");
            }
            else if (!jumpTimerTiming && !didHit)
            {
                inputVel.y = jumpVelocity;
                StartCoroutine(currentJump = jumpTimer());
                //print("regular jump");
            }
        }
    }

    bool jumpTimerTiming = false;
    IEnumerator currentJump;
    IEnumerator jumpTimer()
    {
        jumpTimerTiming = true;
        anim.SetBool("Jump", true);
        //print("jump timer start");
        yield return new WaitForSeconds(jumpTimerLimit);
        jumpTimerTiming = false;
        //print("jump timer end");
        anim.SetBool("Jump", false);
        currentJump = null;
        yield break;
    }

    bool didHit = false;
    IEnumerator doHit()
    {
        didHit = true;
        anim.SetBool("Hit", true);
        //print("Hit");
        hitCollider.gameObject.SetActive(true);
        yield return new WaitForFixedUpdate();
        hitCollider.gameObject.SetActive(false);
        yield return new WaitForSeconds(hitTimerLimit - Time.fixedDeltaTime);
        didHit = false;
        anim.SetBool("Hit", false);
        yield return null;
    }

    Vector3 platformVelocity;
    IEnumerator addOuterVelocity(Vector3 direction, float speed, float time, AnimationCurve velocityCurve)
    {
        if (time <= 0f || health <= 0)
            yield break;
        PlayerState.State = PlayerStateEnum.Uncontrollable;
        direction.Normalize();
        affected = true;
        float t = 0;
        while (t <= time)
        {
            otherSources = velocityCurve.Evaluate(t / time) * direction * speed;
            //print("outer vel: " + otherSources + " speed: " + otherSources.magnitude);
            t += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        if (health > 0)
            PlayerState.State = PlayerStateEnum.Alive;
        else if(PlayerState.State != PlayerStateEnum.Dead)
            PlayerState.State = PlayerStateEnum.Dead;
        affected = false;
    }
    bool affected = false;
    public void AddOuterVelocity(Vector3 direction, float topSpeed, float time, AnimationCurve velocityCurve)
    {
        if (!affected)
        {
            StartCoroutine(addOuterVelocity(direction, topSpeed, time, velocityCurve));
            
        }
    }
    void PlayerDie()
    {
        if (PlayerState.State != PlayerStateEnum.Dead)
        {
            StopAllCoroutines();
            anim.SetBool("Dead", true);
            Scene.main.PlayerLose();
            //rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
    // FixedUpdate is called once per physics frame
    void FixedUpdate()
    {
        PlayerState.Execute();     
    }

    void processGround(RaycastHit hit)
    {
        playerSound.ChangeStepType(hit.collider.tag);
        if((platformsLayer & (1 << hit.collider.gameObject.layer)) != 0)
        {
            Rigidbody otherRig = hit.collider.GetComponent<Rigidbody>();
            if(otherRig)
            {
                platformVelocity = otherRig.velocity;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        CollectibleItem item = other.GetComponent<CollectibleItem>();
        if (item != null)
        {
            item.CollectItem();
        }
    }

    void generateHitCollider()
    {

        if (hitCollider)
            return;

        GameObject g = new GameObject("Hit collider");
        g.transform.SetParent(transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localEulerAngles = Vector3.zero;

        g.layer = 11;

        Mesh m = Utils.GenerateHitMesh(hitColliderData);

        MeshCollider mc = g.AddComponent<MeshCollider>();
        mc.sharedMesh = m;
        mc.convex = true;
        mc.isTrigger = true;

        hitCollider = g.AddComponent<HitCollider>();
        g.SetActive(false);

    }

#if UNITY_EDITOR

    public bool drawDebug = true;

    public bool drawPath = true;

    public bool drawOrientationArrow = true;

    public bool drawHitCollider = true;

    Vector3[] posPast = new Vector3[256];
    int posIndex = 0;
    

    private void OnDrawGizmos()
    {

        if(drawDebug)
        {

            if (drawPath)
            {
                
                Gizmos.color = Color.blue;
                for (int i = 0; i < posPast.Length - 1; i++)
                {
                    if (i + 1 != posIndex)
                        Gizmos.DrawLine(posPast[i], posPast[i + 1]);
                }

            }

            if(drawOrientationArrow)
            {
                Gizmos.color = Color.green;

                Vector3 start = transform.position;
                Vector3 end = Vector3.forward;
                Vector3 midPointR = new Vector3(0.1f, 0f, 0.9f);
                Vector3 midPointL = new Vector3(-0.1f, 0f, 0.9f);

                Vector3 endPoint = start + transform.rotation * end;
                
                Gizmos.DrawLine(start, endPoint);
                Gizmos.DrawLine(endPoint, start + transform.rotation * midPointL);
                Gizmos.DrawLine(endPoint, start + transform.rotation * midPointR);
            }

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position + Vector3.down * sphereCastDistance, sphereCastRadius);

            if(drawHitCollider && hitCollider)
            {

                Mesh m = hitCollider.GetComponent<MeshCollider>().sharedMesh;

                if (didHit)
                    Gizmos.color = Color.red;
                else
                    Gizmos.color = Color.yellow;

                Gizmos.DrawWireMesh(m, 0, hitCollider.transform.position, hitCollider.transform.rotation);

                /*MeshRenderer mr = g.AddComponent<MeshRenderer>();
                mr.material = new Material(Shader.Find("Standard"));*/
            }

        }
    }

#endif

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerSound : MonoBehaviour
{

    [System.Serializable]
    public struct StepData
    {
        public string TagName;
        public AudioClip[] StepClips;

        int CurrentStep;

        public AudioClip GetNextStep()
        {
            if (StepClips == null || StepClips.Length == 0)
                return null;
            return StepClips[CurrentStep++ % StepClips.Length];
        }

    }

    new public AudioSource audio;

    public AudioClip JumpClip;
    public AudioClip HitClip;
    public AudioClip GotHitClip;
    public AudioClip GameLostClip;

    public StepData[] Steps;

    public int CurrentStep = 0;

    private void Start()
    {
        if (!audio)
            audio = GetComponent<AudioSource>();
    }

    public void PlayStep()
    {
        if (PlayerController.main.onGround)
        {
            AudioClip ac = Steps[CurrentStep].GetNextStep();
            audio.PlayOneShot(ac, Scene.EffectsVolume);
        }
    }

    public void ChangeStepType(string groundTag)
    {
        CurrentStep = 0;
        for(int i = 0; i < Steps.Length; i++)
        {
            if(Steps[i].TagName == groundTag)
            {
                CurrentStep = i;
            }
        }
    }

    public void Jump()
    {
        //print("Jump sound...");
        audio.PlayOneShot(JumpClip, Scene.EffectsVolume);
    }

    public void Hit()
    {
        audio.PlayOneShot(HitClip, Scene.EffectsVolume);
    }

    public void GotHit()
    {
        audio.PlayOneShot(GotHitClip, Scene.EffectsVolume);
    }

    public void GameLost()
    {
        audio.PlayOneShot(GameLostClip, Scene.EffectsVolume);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCollider : MonoBehaviour
{


    [System.Serializable]
    public class OnHitEvent : UnityEngine.Events.UnityEvent<Collider>
    {

    }

    public int damage = 1;

    public OnHitEvent onCollide;

    private void OnTriggerEnter(Collider other)
    {
        //print(gameObject.name + " hit: " + other.name);
        IHittable obj = other.GetComponent<IHittable>();
        if(obj != null)
        {
            obj.TakeDamage(damage);
            if (onCollide != null)
                onCollide.Invoke(other);
        }
    }

}

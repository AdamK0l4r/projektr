﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeEnemy : EnemyController
{

    [Min(0.01f)]
    public float shootInterval = 1f;
    public float bulletLifetime = 4f;

    public float rotationSpeed = 180f;

    [Min(0.01f)]
    public float allowedShootAngle = 1f;

    [Min(0f)]
    public float shootHeight = 1f;

    public Vector3 shootPointOffset;
    public GameObject fireObject;

    public float kickOffSpeed = 5f;
    public float kickOffTime = 5f;
    public AnimationCurve kickOffVelocityCurve;

    public float disappearTime = 2f;

    public AudioClip ShootSound;
    public AudioClip DeathSound;

    public AudioSource AudioSource;

    List<GameObject> bullets;

    public Animator anim;

    new void Awake()
    {
        base.Awake();
        EnemyState.StateExitMethods[EnemyStateEnum.Attack] = AttackStop;
        EnemyState.StateEnterMethods[EnemyStateEnum.Die] = OnDie;

        int blc = (int)(Mathf.Max(1, 1f / shootInterval) * bulletLifetime) + 1;
        bullets = new List<GameObject>(blc);
        for (int i = 0; i < blc; i++)
        {
            GameObject o = Instantiate(fireObject, transform);
            o.SetActive(false);
            bullets.Add(o);
        }

    }

    protected override void Guard()
    {
        //print("Slime Guard");
        CheckAttack();
        //just stand...
        anim.SetBool("Attack", false);
    }

    protected override void CheckAttack()
    {
        Vector3 playerPos = PlayerController.main.transform.position;
        //print("In range?");
        if (Vector3.Distance(playerPos, transform.position) < attackCheckRadius && PlayerController.main.PlayerState.State != PlayerController.PlayerStateEnum.Dead)
        {
            
            EnemyState.State = EnemyStateEnum.Attack;
        } else if(health > 0)
        {
            EnemyState.State = EnemyStateEnum.Guard;
        } else
        {
            EnemyState.State = EnemyStateEnum.Die;
        }
    }

    IEnumerator Shooting = null;
    IEnumerator Shoot()
    {
        WaitForSeconds w1 = new WaitForSeconds(shootInterval);
        while(EnemyState.State == EnemyStateEnum.Attack)
        {
            ShootAmmo();
            yield return w1;
        }
        Shooting = null;
    }

    protected override void Attack()
    {

        //print("Slime attack");
        anim.SetBool("Attack", true);
        if (PlayerController.main.PlayerState.State == PlayerController.PlayerStateEnum.Dead)
        {
            EnemyState.State = EnemyStateEnum.Guard;
            //print("Player is dead");
        }

        Vector3 playerPos = PlayerController.main.transform.position;
        if (Vector3.Distance(playerPos, transform.position) > attackCheckRadius)
        {
            EnemyState.State = EnemyStateEnum.Guard;
            //print("Player ran away");
        }

        Vector3 XZLookRotation = playerPos - transform.position;
        XZLookRotation.y = 0;

        Quaternion rotation = Quaternion.LookRotation(XZLookRotation);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotationSpeed * Time.fixedDeltaTime);

        float angle = Quaternion.Angle(transform.rotation, rotation);

        if (angle <= allowedShootAngle)
        {
            //print("Shoot?: ");
            if (Shooting == null)
            {
                StartCoroutine(Shooting = Shoot());
            }
        }
        else
        {
            StopAllCoroutines();
            Shooting = null;
            //print("Off angle");
        }

    }

    void AttackStop()
    {
        //shooting = false;
        StopAllCoroutines();
        Shooting = null;
    }

    int currentBullet = 0;
    void ShootAmmo()
    {
        //print(currentBullet);
        AudioSource.PlayOneShot(ShootSound, Scene.EffectsVolume);
        bullets[currentBullet].GetComponent<Bullet>().Fire(transform.TransformPoint(shootPointOffset), PlayerController.main.transform.position, shootHeight);
        //print("Bullet Fired");
        currentBullet = (currentBullet + 1) % bullets.Count;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.TransformPoint(shootPointOffset), 0.1f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (EnemyState.State != EnemyStateEnum.Die)
        {
            PlayerController p = collision.collider.gameObject.GetComponent<PlayerController>();
            if (p != null)
            {
                p.TakeDamage(2);
                p.AddOuterVelocity((p.transform.position - transform.position), kickOffSpeed, kickOffTime, kickOffVelocityCurve);
            }
        }
    }

    void OnDie()
    {
        StartCoroutine(Dissapear());
        anim.SetBool("Dead", true);
        AudioSource.PlayOneShot(DeathSound, Scene.EffectsVolume);
    }

    IEnumerator Dissapear()
    {
        WaitForSeconds w1 = new WaitForSeconds(disappearTime);
        yield return w1;
        gameObject.SetActive(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleKey : CollectibleItem
{
    public Gate gate;
    void Start()
    {
        
    }
    public override void CollectItem()
    {

        if (OnCollectSound)
        {
            Source.PlayOneShot(OnCollectSound, Scene.EffectsVolume);
        }

        gate.OpenGate();
        this.gameObject.SetActive(false);
    }
}

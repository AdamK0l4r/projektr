﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour, IHittable
{

    public enum EnemyStateEnum
    {
        Guard,
        Attack,
        Die
    }

    public int health = 1;

    public float speed = 2f;

    public float pointGuidingInterval = 0.5f;

    public float attackCheckRadius = 5f;
    public float attackCheckDistance = 5f;

    public float attackRefreshInterval = 0.5f;

    [System.Serializable]
    public struct GuardPoint
    {
        public float WaitTime;
        public Vector3 Position;
    }

    public GuardPoint[] guardPoints;

    protected StateMachine<EnemyStateEnum> EnemyState;

    protected NavMeshAgent agent;

    protected int currentGuardPoint = 0;

    public virtual void TakeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            EnemyState.State = EnemyStateEnum.Die;
        }
    }

    protected void Awake()
    {

        EnemyState = new StateMachine<EnemyStateEnum>(EnemyStateEnum.Guard, new System.Action[]
        {
            Guard,
            Attack,
            Die
        });

        EnemyState.StateEnterMethods[EnemyStateEnum.Attack] = ToAttack;

        agent = GetComponent<NavMeshAgent>();

        agent.speed = speed;

        if (guardPoints == null)
            guardPoints = new GuardPoint[1];

        if (guardPoints.Length == 0)
            guardPoints = new GuardPoint[1];

        for(int i = 0; i < guardPoints.Length; i++)
        {
            guardPoints[i].Position += transform.position;
        }

    }

    void FixedUpdate()
    {
        EnemyState.Execute();
    }

    protected virtual void CheckAttack()
    {

        if (PlayerController.main.PlayerState.State == PlayerController.PlayerStateEnum.Dead)
            return;

        Vector3 playerPos = PlayerController.main.transform.position;
        if(Vector3.Distance(playerPos, transform.position) < attackCheckRadius)
        {
            NavMeshPath path = new NavMeshPath();
            if(agent.CalculatePath(playerPos, path))
            {
                float dist = 0f;
                for(int i = 0; i < path.corners.Length - 1; i++)
                {
                    dist += Vector3.Distance(path.corners[i], path.corners[i + 1]);
                }
                if(dist < attackCheckDistance)
                {
                    EnemyState.State = EnemyStateEnum.Attack;
                    //print("Enemy attack");
                    return;
                }
            }
        }

    }

    protected float pointDistanceError = 1f;

    protected float attackTimer = 0f;

    bool Guarding = false;
    IEnumerator GuardGivenPoints()
    {

        Guarding = true;

        //print("Gurad on");

        while (EnemyState.State == EnemyStateEnum.Guard)
        {

            //print(EnemyState.State);

            while (Vector3.Distance(guardPoints[currentGuardPoint].Position, transform.position) > pointDistanceError && EnemyState.State == EnemyStateEnum.Guard)
            {
                agent.SetDestination(guardPoints[currentGuardPoint].Position);
                yield return new WaitForSeconds(pointGuidingInterval);
            }

            //print("wait: " + guardPoints[currentGuardPoint].WaitTime);

            yield return new WaitForSeconds(guardPoints[currentGuardPoint].WaitTime > 0f ? guardPoints[currentGuardPoint].WaitTime : 1f);

            currentGuardPoint = (currentGuardPoint + 1) % guardPoints.Length;

        }

        //print("Guard off");

        Guarding = false;

    }

    #region STATE_METHODS

    protected virtual void Guard()
    {
        CheckAttack();

        if (!Guarding)
            StartCoroutine(GuardGivenPoints());

    }


    protected virtual void Attack()
    {
        Vector3 playerPosition = PlayerController.main.transform.position;

        if (Vector3.Distance(playerPosition, transform.position) > attackCheckDistance)
        {
            EnemyState.State = EnemyStateEnum.Guard;
            return;
        }

        if(attackTimer >= attackRefreshInterval)
        {
            attackTimer = 0f;
            agent.SetDestination(PlayerController.main.transform.position);
            //print("attaccc");
        }
        attackTimer += Time.fixedDeltaTime;
    }

    protected virtual void Die()
    {
        //dead or dying
    }

    #endregion

    #region STATE_SWITCHES

    protected virtual void ToAttack()
    {
        attackTimer = 0f;
    }

    #endregion

}

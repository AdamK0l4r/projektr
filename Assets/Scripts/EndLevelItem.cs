﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelItem : CollectibleItem
{

    public override void CollectItem()
    {

        if (OnCollectSound)
        {
            Source.PlayOneShot(OnCollectSound, Scene.EffectsVolume);
        }

        Scene.main.PlayerWin();
        gameObject.SetActive(false);
    }

}
